const productService = require('../servicios/products');

function getProducts(req, res) {
	productService.getAll()
		.then(data => res.send(data));
};

function getProduct(req, res) {
	productService.getById(req.params.id)
		.then(data => res.send(data));
}

function addProducts(req, res) {
	productService.addProducts({
			name: req.body.name,
            sku: req.body.sku,
            description: req.body.description,
            price: req.body.price,
            stock: req.body.stock,
            category_id: req.body.category_id
		})
		.then(data => res.send(data));
};

function updateProducts(req, res) {
	productService.updateProducts({
        id: req.body.id,
        name: req.body.name,
        sku: req.body.sku,
        description: req.body.description,
        price: req.body.price,
        stock: req.body.stock,
        category_id: req.body.category_id
		})
		.then(data => res.send(data));
};

function deleteProducts(req, res) {
	res.send(productService.deleteProducts({
		id: req.params.id
	}));
};


module.exports = {
    getProducts,
    getProduct,
    addProducts,
    updateProducts,
    deleteProducts
}