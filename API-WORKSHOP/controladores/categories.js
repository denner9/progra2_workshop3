const categoryService = require('../servicios/category');

function getCategories(req, res) {
	categoryService.getAll()
		.then(data => res.send(data));
};

function getCategorie(req, res) {
	categoryService.getById(req.params.id)
		.then(data => res.send(data));
}

function addCategories(req, res) {
	categoryService.addCategory({
			name: req.body.name,
			description: req.body.description
		})
		.then(data => res.send(data));
};

function updateCategories(req, res) {
	categoryService.updateCategory({
			id: req.body.id,
			name: req.body.name,
			description: req.body.description
		})
		.then(data => res.send(data));
};

function deleteCategories(req, res) {
	res.send(categoryService.deleteCategory({
		id: req.params.id
	}));
};


module.exports = {
	getCategories,
	getCategorie,
	addCategories,
	updateCategories,
	deleteCategories
}