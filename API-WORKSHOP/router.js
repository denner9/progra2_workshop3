const categoryController = require('./controladores/categories');
const productController = require('./controladores/products');
var basicAuth = require('basic-auth');

module.exports.set = (app) => {

    var auth = function(req, res, next) {
        var user = basicAuth(req);
        if (!user || !user.name || !user.pass) {
            res.set('WWW-Authenticate', 'Basic realm=Authorization Required');
            res.sendStatus(401);
            return;
        }
        if (user.name === 'kevin' && user.pass === '123') {
            next();
        } else {
            res.set('WWW-Authenticate', 'Basic realm=Authorization Required');
            res.sendStatus(401);
            return;
        }
    }

    app.post('/category', auth, categoryController.addCategories);
    app.get('/category', auth, categoryController.getCategories);
    app.patch('/category', auth, categoryController.updateCategories);
    app.delete('/category/:id', auth, categoryController.deleteCategories);
    app.get('/category/:id', auth, categoryController.getCategorie);

    app.post('/product', auth, productController.addProducts);
    app.get('/product', auth, productController.getProducts);
    app.patch('/product', auth, productController.updateProducts);
    app.delete('/product/:id', auth, productController.deleteProducts);
    app.get('/product/:id', auth, productController.getProduct);
}