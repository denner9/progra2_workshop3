const Sequelize = require('sequelize');
const sequelize = require('../db');

const Category = sequelize.define('categorie', {
    id: {
		type: Sequelize.INTEGER,
		primaryKey: true,
		autoIncrement: true
	},
    name: {
        type: Sequelize.STRING
    },
    description: {
        type: Sequelize.STRING
    }
});

const Products = sequelize.define('product', {
    id: {
		type: Sequelize.INTEGER,
		primaryKey: true,
		autoIncrement: true
	},
    name: {
        type: Sequelize.STRING
    },
    sku: {
        type: Sequelize.INTEGER
    },
    description: {
        type: Sequelize.STRING
    },
    price: {
        type: Sequelize.INTEGER
    },
    stock: {
        type: Sequelize.INTEGER
    },
    category_id: {
        type: Sequelize.INTEGER
    }
});


module.exports = {
    Category,
    Products
}