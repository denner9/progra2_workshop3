module.exports = {
        port: 8000,
        ip: '0.0.0.0',
        dbConnectionString: "postgres://postgres:123@localhost:5432/workshop",
        operatorsAliases: false,
        saltRounds: 2,
        jwtSecret: 'yocreoenlautn',
        tokenExpireTime: '2w'
    }
    //No salvar este archivo es "peligroso"