const Categories = require('../modelos').Category;

const getAll = () => Categories.findAll();
const getById = id => Categories.findById(id);
const addCategory = category => Categories.create(category);
const updateCategory = category => Categories.update(category, {
    where: {
        id: category.id
    }
});

const deleteCategory = category => {
    Categories.findById(category.id).then((result) => {
        console.log(result);
        return Categories.destroy({
                where: {
                    id: category.id
                }
            })
            .then((u) => {
                return result
            });

    });

}

module.exports = {
    getAll,
    getById,
    addCategory,
    updateCategory,
    deleteCategory
};