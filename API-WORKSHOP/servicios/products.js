const Products = require('../modelos').Products;

const getAll = () => Products.findAll();
const getById = id => Products.findById(id);
const addProducts = product => Products.create(product);
const updateProducts = product => Products.update(product, {
    where: {
        id: product.id
    }
});

const deleteProducts = product => {
    Products.findById(product.id).then((result) => {
        console.log(result);
        return Products.destroy({
                where: {
                    id: product.id
                }
            })
            .then((u) => {
                return result
            });

    });

}

module.exports = {
    getAll,
    getById,
    addProducts,
    updateProducts,
    deleteProducts
};