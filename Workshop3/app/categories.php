<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class categories extends Model
{
    protected $fillable = ['id', 'name', 'description'];
    public $timestamps = false;
}
